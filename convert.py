import json
from uuid import uuid4
import shutil
import sys
import pandas as pd

ods_path = sys.argv[1]
first_language = sys.argv[2]
second_language = sys.argv[3]

shutil.copy(ods_path, ods_path + ".bckp")
data = pd.read_excel(ods_path, engine="odf")

json_sets = {}

for index, row in data.iterrows():
    if row["source"] not in json_sets:
        json_sets[row["source"]] = {
            "id": str(uuid4()),
            "name": row["source"],
            "description": "",
            "baseLanguage": first_language,
            "targetLanguage": second_language,
            "source": "",
            "level": "A1",
            "entries": []
        }
    entries = json_sets[row["source"]]["entries"]
    entries.append({
        "baseLanguageExpression": str(row[first_language]).split(";"),
        "targetLanguageExpression": str(row[second_language]).split(";"),
        "note": row["note"] if str(row["note"]) != "nan" else None,
        "numberOfSuccesses": row["number_of_success"],
        "numberOfAllSuccesses": row["number_of_all_success"],
        "numberOfFailures": row["number_of_failures"],
        "numberOfAllFailures": row["number_of_all_failures"],
        "lastSuccessTimestamp": row["last_success_timestamp"] if str(row["last_success_timestamp"]) != "nan" else None,
        "lastFailureTimestamp": row["last_failure_timestamp"] if str(row["last_failure_timestamp"]) != "nan" else None,
    })

for json_set in json_sets.values():
    with open(f"output/{json_set['id']}.json", "w") as f:
        json.dump(json_set, f)
