import os
import shutil
import sys
import re
import pandas as pd
import numpy as np
from random import randint
from rapidfuzz import fuzz

ods_path = sys.argv[1]
first_language = sys.argv[2]
second_language = sys.argv[3]
number_of_required_success = 3
days_to_recheck = 90
min_score_to_accept = 99
min_score_to_ask_typo = 90

shutil.copy(ods_path, ods_path + ".bckp")
data = pd.read_excel(ods_path, engine="odf")
data["last_success_timestamp"] = pd.to_datetime(data["last_success_timestamp"])
data["last_failure_timestamp"] = pd.to_datetime(data["last_failure_timestamp"])

try:
    lessons = list(data.source.unique())
    lessons.append("All")
    lessons.sort()
    print("Lessons:")
    for lesson in lessons:
        print("  -", lesson)
    selected_lesson = input("Select a lesson: ")

    while True:
        os.system('clear' if os.name == 'posix' else 'cls')

        date_to_recheck = pd.Timestamp('now').floor('D') + pd.offsets.Day(-days_to_recheck)
        possible_questions = data.loc[
            (data["number_of_success"] < number_of_required_success) | 
             (data["last_success_timestamp"].isnull()) |
             (data["last_success_timestamp"] < date_to_recheck)
        ]
        if selected_lesson != "All":
            possible_questions = possible_questions.loc[data["source"] == selected_lesson]

        if len(possible_questions.index) == 0:
            print("Everything is learned. Exiting...")
            break

        question = possible_questions.sample()
        for index, row in question.iterrows():
            method = randint(0, 1)
            correct_answer = ""
            is_ok = False

            if method == 0:
                source_language = first_language
                target_language = second_language
            else:
                source_language = second_language
                target_language = first_language

            possible_answers = row[target_language].split(";")
            original_answers = row[target_language].split(";")
            for i, answer in enumerate(possible_answers):
                prepared_answer = answer

                prepared_answer = re.sub(r"\(.*\)", "", prepared_answer)

                num_subs_made = 1
                while num_subs_made > 0:
                    prepared_answer, num_subs_made = re.subn(r"  ", " ", prepared_answer)

                prepared_answer = prepared_answer.strip()
                
                possible_answers[i] = prepared_answer.lower()

            print("How do you say it in ", target_language, ": ", row[source_language])
            solution = input()
            answer_scores = list(map(lambda a: fuzz.ratio(solution.lower(), a), possible_answers))
            max_score = max(answer_scores)
            max_score_index = answer_scores.index(max_score)

            if max_score >= min_score_to_accept:
                is_ok = True
            elif max_score >= min_score_to_ask_typo:
                print("The correct answer is", original_answers[max_score_index])
                typo_answer = input("Have you made a typo? (y/n): ")
                is_ok = typo_answer == "y"

            correct_answer = row[target_language]

            print()
            if is_ok:
                number_of_success = data.at[index, "number_of_success"] + 1
                if number_of_success > number_of_required_success:
                    number_of_success = number_of_required_success
                data.at[index, "number_of_success"] = number_of_success
                data.at[index, "number_of_all_success"] += 1
                data.at[index, "number_of_failures"] = 0
                data.at[index, "last_success_timestamp"] = pd.Timestamp('now')
                print("OK")
            else:
                number_of_success = data.at[index, "number_of_success"]
                number_of_success -= 2
                if number_of_success < 0:
                    number_of_success = 0
                data.at[index, "number_of_success"] = number_of_success
                data.at[index, "number_of_failures"] += 1
                data.at[index, "number_of_all_failures"] += 1
                data.at[index, "last_failure_timestamp"] = pd.Timestamp('now')
                if len(original_answers) > 1:
                    print("NOK the answer was one of", str.join("; ", original_answers))
                else:
                    print("NOK the answer was", original_answers[0])

            input()

except KeyboardInterrupt:
    print("Exiting ...")
    pass
finally:
    data["last_success_timestamp"] = data["last_success_timestamp"].map(lambda x: x.isoformat())
    data["last_failure_timestamp"] = data["last_failure_timestamp"].map(lambda x: x.isoformat())
    data.to_excel(ods_path, index=False)
