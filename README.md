# Wordie

Wordie is a simple word learning tool. It gets a spreadsheet as input and
maintains some statistics of the learning progress in the same spreadsheet.
This single file approach makes it easier to use.

# Spreadsheet format

The spreadsheet must have at least the following columns:
    - source
    - number_of_success
    - number_of_failures
    - last_success_timestamp
    - last_failure_timestamp
    - number_of_all_success
    - number_of_all_failures

The columns starting with number_of must be initialized with 0.

Furthermore, there must be two additional columns with the two languages. For
example DE and EN for german and english.

# Usage

```bash
python3 main.py <path-to-ods> <first language column> <second language column>
```
